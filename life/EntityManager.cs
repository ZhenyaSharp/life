﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace life
{
    class EntityManager
    {
        private Random rnd;
        private int minX, minY, maxX, maxY;
        private Entity[] entities;

        public EntityManager(int minX, int minY, int maxX, int maxY, int countEntities, int hitPoint, string sex)
        {
            this.rnd = new Random();
            this.minX = minX;
            this.minY = minY;
            this.maxX = maxX;
            this.maxY = maxY;
            this.entities = new Entity[countEntities];
            InitEntity();
        }

        private void InitEntity()
        {
            for (int i = 0; i < entities.Length; i++)
            {
                entities[i] = new Entity(rnd.Next(minX, maxX + 1), rnd.Next(minY, maxY + 1), rnd.Next(0, 2), rnd.Next(50, 101));
            }
        }

        private void Step()
        {
            for (int i = 0; i < entities.Length; i++)
            {
                int direction = rnd.Next(0, 4);
                Move(entities[i], direction);
            }
        }

        private void Move(Entity entities, int direction)
        {
            switch (direction)
            {
                case 0:
                    if (entities.GetX() > minX)
                    {
                        entities.MoveLeft();
                    }
                    break;
                case 1:
                    if (entities.GetY() > minY)
                    {
                        entities.MoveUp();
                    }
                    break;
                case 2:
                    if (entities.GetX() < maxX)
                    {
                        entities.MoveRight();
                    }
                    break;
                case 3:
                    if (entities.GetY() < maxY)
                    {
                        entities.MoveDown();
                    }
                    break;
            }
        }

        private void DrawEntity()
        {
            for (int i = 0; i < entities.Length; i++)
            {
                entities[i].Draw();
            }
        }

        private void ResizeArray(ref Entity[] entities, int newLength)
        {
            int minLength;

            minLength = newLength > entities.Length ? entities.Length : newLength;

            Entity[] newArray = new Entity[newLength];

            for (int i = 0; i < minLength; i++)
            {
                newArray[i] = entities[i];
            }
            entities = newArray;
        }

        private void AddNewEntity(ref Entity[] entities, Entity entity)
        {
            ResizeArray(ref entities, entities.Length + 1);
            entities[entities.Length - 1] = entity;
        }

        private void DeleteEntity(ref Entity[] entities, int index)
        {
            Entity[] newEntities = new Entity[entities.Length - 1];
            int newI = 0;

            for (int i = 0; i < entities.Length; i++)
            {
                if (i != index)
                {
                    newEntities[newI] = entities[i];
                    newI++;
                }
            }

            entities = newEntities;
        }

        private void FindCollision()
        {
            for (int i = 0; i < entities.Length; i++)
            {
                for (int j = i; j < entities.Length; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }

                    if (entities[i].GetX() == entities[j].GetX() && entities[i].GetY() == entities[j].GetY())
                    {
                        if (entities[i].GetSex() != entities[j].GetSex())
                        {
                            Entity newObj = new Entity(rnd.Next(minX, maxX + 1), rnd.Next(minY, maxY + 1),
                                rnd.Next(0, 2), rnd.Next(50, 101));
                            AddNewEntity(ref entities, newObj);
                        }
                        else
                        {
                            if (entities[i].GetHealth() > entities[j].GetHealth())
                            {
                                entities[i].SetHealth(entities[i].GetHealth() + entities[j].GetHealth() / 2);
                                DeleteEntity(ref entities, j);
                            }
                            else
                            {
                                entities[j].SetHealth(entities[j].GetHealth() + entities[i].GetHealth() / 2);
                                DeleteEntity(ref entities, i);
                            }
                        }
                    }
                }
            }
        }


        public void Run()
        {
            while (true)
            {
                Step();
                DrawEntity();
                FindCollision();
                System.Threading.Thread.Sleep(180);
                Console.Clear();
            }
        }
    }
}
