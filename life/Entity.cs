﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace life
{
    class Entity
    {
        private int x, y;
        private ConsoleColor color;
        private int sex;
        private int health;

        public Entity()
        {
            x = y = 0;
            color = ConsoleColor.Magenta;
            sex = 0;
            health = 100;
        }

        public Entity(int x, int y, int sex, int health)
        {
            this.x = x;
            this.y = y;
            this.color = sex == 0 ? ConsoleColor.Magenta : ConsoleColor.Blue;
            this.sex = sex;
            this.health = health;
        }

        public void Draw()
        {
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = color;
            Console.Write(sex == 0 ? 'F' : 'M');
        }

        public void MoveDown()
        {
            y += 1;
        }

        public void MoveUp()
        {
            y -= 1;
        }

        public void MoveRight()
        {
            x += 1;
        }

        public void MoveLeft()
        {
            x -= 1;
        }

        public void SetX(int x)
        {
            this.x = x;
        }

        public int GetX()
        {
            return x;
        }

        public void SetY(int y)
        {
            this.y = y;
        }

        public int GetY()
        {
            return y;
        }

        public int GetSex()
        {
            return sex;
        }

        public void SetHealth(int health)
        {
            this.health = health;
        }

        public int GetHealth()
        {
            return health;
        }

    }
}
